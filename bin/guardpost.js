#! /usr/bin/env node

var GuardPost = require('../lib/guardpost');

var gp = new GuardPost({ 
    api_key: 'pubkey-5ogiflzbnjrljiky49qxsiozqef5jxp7'
  });

var email = process.argv.slice(2).join();

gp.validate(email, function (err, valid, data) {
  if (err) { 
    console.log(err);
    return;
  }
  console.log("Email '" + data.address +"' is "+ (valid ? "valid" : "invalid"));
  console.log(JSON.stringify(data, null, 2));
})