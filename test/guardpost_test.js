var chai = require('chai');
var expect = chai.expect;
var Faker = require('fakerjs');

// test subject.
var GuardPost = require('../lib/guardpost')


describe("Guardpost Nodejs Client", function() {

  var email;
  var api;

  beforeEach(function (done){
    
    api = new GuardPost(
        { api_key: 'pubkey-5ogiflzbnjrljiky49qxsiozqef5jxp7' }
      );
    done();
  });
  

  it("accepts a valid email", function (done){
    email = "foo@mailgun.net";
    api.validate(email, function (err, valid, data) {
      expect(err).to.be.null;
      expect(valid).to.be.true;
      expect(data).to.deep.equal(
        {
          "address": email,
          "did_you_mean": null,
          "is_valid": true,
          "parts": {
            "display_name": null, //Deprecated Field, will always be null
            "domain": "mailgun.net",
            "local_part": "foo"
          }
        }
      );
      done();
    });
  });

  it("gives suggestion email on probable typo", function (done){
    email = "foo@maiglun.net";
    api.validate(email, function (err, valid, data) {
      expect(err).to.be.null;
      expect(valid).to.be.false;
      expect(data).to.deep.equal(
        {
          "address": email,
          "did_you_mean": "foo@mailgun.net",
          "is_valid": false,
          "parts": {
            "display_name": null, //Deprecated Field, will always be null
            "domain": null,
            "local_part": null
          }
        }
      );
      done();
    });
  });

  // timeout of 200 gives request time to finish
  // lower triggers timeout event
  // timeout of 0 disables timing out.
  it("falls back to basic validation on timeout", function (done){
    email = "foo@mailgun.net";
    api.config({timeout:1});
    api.validate(email, function (err, valid, data) {
      expect(err).to.be.null;
      expect(valid).to.be.true;
      expect(data).to.deep.equal(
        {
          "address": email,
          "did_you_mean": null,
          "is_valid": true,
          "parts": {
            "display_name": null, //Deprecated Field, will always be null
            "domain": "mailgun.net",
            "local_part": "foo"
          }
        }
      );
      done();
    });
  });


  it("does not suggest on timeout", function (done){
    email = "foo@maiglun.net";
    api.config({timeout:1});
    api.validate(email, function (err, valid, data) {
      expect(err).to.be.null;
      expect(valid).to.be.true;
      expect(data).to.deep.equal(
        {
          "address": email,
          "did_you_mean": null,
          "is_valid": true,
          "parts": {
            "display_name": null, //Deprecated Field, will always be null
            "domain": "maiglun.net",
            "local_part": "foo"
          }
        }
      );
      done();
    });
  });

});