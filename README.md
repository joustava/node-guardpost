# node-guardpost

Module for using Mailguns [Guardpost](http://blog.mailgun.com/free-email-validation-api-for-web-forms/) email validation service from a Node application.

## Installation

    // will become npm install node-guardpost --save
    npm install git+ssh://git@bitbucket.org:joustava/node-guardpost.git --save

## Usage

```js
// Require the lib
var gp = require('node-guardpost');

// Call with config object, the email to validate and a callback function.
config = {
    // Api Key from http://documentation.mailgun.com/api-email-validation.html#example
    api_key: 'pubkey-5ogiflzbnjrljiky49qxsiozqef5jxp7',
};

gp(config, email, function(err, valid, data){
    if(err){
        // Handle error;
        return
    }
    if(valid){
        // Handle valid case
    } else {
        // Handle invalid case
    }
});
```

The *data* param will contain the information returned by the guardpost service. It will look something like this when an email is deemed valid.

```json
{
    "address": foo@email.net,
    "did_you_mean": null,
    "is_valid": true,
    "parts": {
        "display_name": null, //Deprecated Field, will always be null
         "domain": "mailgun.net",
         "local_part": "foo"
    }
}
```

The api will fall back to basic validation when a timeout of about 1000 ms is reached. When you like to wait longer due to e.g a slow connection, supply a 'timeout' parameter with time in milliseconds to the config object. 

Check the tests for more insight.

## Tests

    npm test

## Versioning

For handling versioning we use [semver-sync](https://github.com/cimi/semver-sync).

## Contributing

In lieu of a formal styleguide, take care to maintain the existing coding style. 
Add unit tests for any new or changed functionality. Lint and test your code.

## Release History

* 0.1.5 Initial release