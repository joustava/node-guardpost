var validator = require("validator");

// Merge supplied options with defualts.
exports.merge = function (defaults) {
  var i;
  var opt;

  for (i = 1; i < arguments.length; i++) {
    for (opt in arguments[i]) {
      if (arguments[i].hasOwnProperty(opt)) {
        defaults[opt] = arguments[i][opt];
      }
    }
  }

  return defaults;
};

// Fallback response to basic validation using node-validate lib.
exports.fallback = function(address_text) {
  return {
    address: address_text,
    did_you_mean: null,
    is_valid: validator.isEmail(address_text),
    parts: {
      display_name: null,
      domain: address_text.split('@')[1],
      local_part: address_text.split('@')[0]
    }
  }
}
