var https = require('https');
var utils = require('./utils');
var fs    = require('fs');
var keys  = require('./keys');

var GuardPost = function (options) {
 
  if(!('api_key' in options)){
    throw new Error('Api key required. Please supply your api_key to node-guardpost.');
  }

  this.options = utils.merge(options, keys);
};


GuardPost.Version = JSON.parse(fs.readFileSync(__dirname + '/../package.json')).version;

module.exports = GuardPost;

GuardPost.prototype.config = function (config) {
  this.options = utils.merge(this.options, config);
}

GuardPost.prototype.get = function(address_text, done) {
  var _this = this;

  console.log(this.options);
  if(!(done instanceof Function)){
    throw new Error('Invalid callback.');
    return this;
  }

  if (!address_text) { 
    throw new TypeError('Guardpost needs an email address');
  }
  
  if (!done) { 
    throw new TypeError('Guardpost needs a callback to return you the result');
  }
  
  if (address_text.length > 512) {
    return done( new Error('Stream exceeds maximum allowable length of 512.'));
  }

  // Trigger some in-progress indicator.
  // if (inprogress) {
  //   inprogress();
  // }

  

  var _options = {
    headers: {
      'Authorization': 'Basic ' + new Buffer('api'+_this.options.api_key).toString('base64'),
      'Accept': 'application/json'
    },
    hostname: _this.options.hostname,
    path: _this.options.path + address_text
  };

  // Make the call to Guardpost.
  var req = https.request(_options, function (res) {
    res.setEncoding('utf8');
    // Listen for incoming data.
    res.on('data', function (chunk) {
      var data = JSON.parse(chunk);
      done(null, data.is_valid, data);
    });

    // Listen for response errors.
    res.on('error', function (chunk) {
      console.log('res error');
      done(chunk);
    });
  });

  req.on('socket', function (socket) {
    socket.setTimeout(_this.options.timeout);  
    socket.on('timeout', function() {
      req.abort();
    });
  });

  // Listen for request errors.
  req.on('error', function (err) {
    if (err.code === "ECONNRESET") {
      var data = utils.fallback(address_text);
      return done(null, data.is_valid, data);
    }
    done(err);
  });

  // Make the request.
  req.end();
}

// Validate email against Guardpost
// String - email to validate
// Function - callback(err, valid, data) signature.
//
GuardPost.prototype.validate = function (email, callback) {
  this.get(email, callback);
  return this;
};